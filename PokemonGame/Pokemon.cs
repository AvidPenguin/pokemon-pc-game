﻿using System;

namespace PokemonGame
{
    class Pokemon
    {
        public string Name { get; set; }
        public int ID { get; set; }
        public Type Type1 { get; set; }
        public Type Type2 { get; set; }
        public string EvolvesFrom { get; set; }
        public string Nickname { get; set; }
        public Gender Gender { get; set; }
        public bool Shiny { get; set; }
        public double MaleChance { get; set; }
        public int CatchRate { get; set; }
        public int Level { get; set; }
        public int CurrentHP { get; set; }
        public int HP { get; set; }
        public int Attack { get; set; }
        public int Defence { get; set; }
        public int SpAttack { get; set; }
        public int SpDefence { get; set; }
        public int Speed { get; set; }


        public Pokemon()
        {
            CatchRate = 20;
        }

        public bool ThrowPokeball()
        {
            Random rng = new Random();
            int catchrate = rng.Next(100)+1;
            if (catchrate <= CatchRate)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public void SetType1(string type)
        {
            Type1 = ToType(type);
        }

        public void SetType2(string type)
        {
            Type2 = ToType(type);
        }

        private Gender CalcGender(double malechance)
        {
            Random rng = new Random();
            int percentage = rng.Next(1, 100);
            if (malechance >= percentage)
            {
                return Gender.Male;
            }
            else
            {
                return Gender.Female;
            }
        }
        
        public void GenerateIndependantValues()
        {
            Gender = CalcGender(MaleChance);
            Shiny = false;
        }

        private Type ToType(string type)
        {
            switch (type.ToLower())
            {
                case "normal":
                    return Type.Normal;
                case "fire":
                    return Type.Fire;
                case "water":
                    return Type.Water;
                case "electric":
                    return Type.Electric;
                case "grass":
                    return Type.Grass;
                case "ice":
                    return Type.Ice;
                case "fighting":
                    return Type.Fighting;
                case "poison":
                    return Type.Poison;
                case "ground":
                    return Type.Ground;
                case "flying":
                    return Type.Flying;
                case "psychic":
                    return Type.Psychic;
                case "bug":
                    return Type.Bug;
                case "rock":
                    return Type.Rock;
                case "ghost":
                    return Type.Ghost;
                case "dragon":
                    return Type.Dragon;
                case "dark":
                    return Type.Dark;
                case "steel":
                    return Type.Steel;
                case "fairy":
                    return Type.Fairy;
                default:
                    return Type.Null;
            }
        }
    }



    enum Type
    {
        Normal,
        Fire,
        Water,
        Electric,
        Grass,
        Ice,
        Fighting,
        Poison,
        Ground,
        Flying,
        Psychic,
        Bug,
        Rock,
        Ghost,
        Dragon,
        Dark,
        Steel,
        Fairy,
        Null
    };

    enum Gender
    {
        Male,
        Female,
        Genderless
    }
}
