﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PokemonGame
{
    public partial class Main : Form
    {
        private string[] datasrc { get; set; }
        private Pokemon pkmn { get; set; }
        private List<Pokemon> AllPokemon = new List<Pokemon>();
        private List<Pokemon> PokemonList = new List<Pokemon>();
        private List<Location> LocationList = new List<Location>();
        private bool BattleMode = false;

        public static int money = 1000;
        public static int pokeballs = 10;
        public static int greatballs = 0;
        public static int ultraballs = 0;
        Random rng = new Random();
        public Location currentLocation;

        public Main()
        {
            InitializeComponent();
            InitializePokemon();
            InitializeLocations();
            PokemonList.Add(CreateNewPokemon(7));
            UpdateUI();
        }

        private void UpdateUI()
        {
            UpdateTracker();

            btnRun.Enabled = BattleMode;
            btnBattle.Enabled = !BattleMode;

            l_money.Text = "£" + money.ToString();
            l_pokeballs.Text = pokeballs.ToString();
            l_greatballs.Text = greatballs.ToString();
            l_ultraballs.Text = ultraballs.ToString();

            l_location.Text = currentLocation.name;

            if (!BattleMode)
            {
                if (currentLocation.north != null)
                {
                    btnNorth.Text = currentLocation.north;
                    btnNorth.Enabled = true;
                }
                if (currentLocation.east != null)
                {
                    btnEast.Text = currentLocation.east;
                    btnEast.Enabled = true;
                }
                if (currentLocation.south != null)
                {
                    btnSouth.Text = currentLocation.south;
                    btnSouth.Enabled = true;
                }
                if (currentLocation.west != null)
                {
                    btnWest.Text = currentLocation.west;
                    btnWest.Enabled = true;
                }

                if (currentLocation.north == null)
                {
                    btnNorth.Text = "";
                    btnNorth.Enabled = false;
                }
                if (currentLocation.east == null)
                {
                    btnEast.Text = "";
                    btnEast.Enabled = false;
                }
                if (currentLocation.south == null)
                {
                    btnSouth.Text = "";
                    btnSouth.Enabled = false;
                }
                if (currentLocation.west == null)
                {
                    btnWest.Text = "";
                    btnWest.Enabled = false;
                }


                btnPokeBall.Enabled = false;

            }
            else
            {
                btnNorth.Enabled = false;
                btnEast.Enabled = false;
                btnSouth.Enabled = false;
                btnWest.Enabled = false;
                btnPokeBall.Enabled = true;
            }

            if (pkmn == null)
            {
                picType1.Image = null;
                picType2.Image = null;
                picType3.Image = null;
                picSprite.Image = null;
                l_appeared.Text = null;
                l_gender.Text = null;
                l_level.Text = null;
            }
            else
            {
                l_appeared.Text = "A wild " + pkmn.Name + " appeared!";
                l_gender.Text = pkmn.Gender.ToString();
                l_level.Text = "Level " + pkmn.Level.ToString();

                if (pkmn.Gender == Gender.Female)
                {
                    l_gender.ForeColor = Color.Purple;
                }
                else if (pkmn.Gender == Gender.Male)
                {
                    l_gender.ForeColor = Color.Blue;
                }
                else
                {
                    l_gender.ForeColor = Color.Black;
                }

                object imgSprite = Properties.Resources.ResourceManager.GetObject(pkmn.ID.ToString());
                picSprite.Image = (Image)imgSprite;


                object imgType1 = Properties.Resources.ResourceManager.GetObject(pkmn.Type1.ToString().ToLower());
                picType1.Image = (Image)imgType1;


                object imgType2 = Properties.Resources.ResourceManager.GetObject(pkmn.Type2.ToString().ToLower());

                if (pkmn.Type2 == Type.Null)
                {
                    picType1.Hide();
                    picType2.Hide();
                    picType3.Show();
                    picType3.Image = (Image)imgType1;
                }
                else
                {
                    picType1.Show();
                    picType2.Show();
                    picType3.Hide();
                    picType1.Image = (Image)imgType1;
                    picType2.Image = (Image)imgType2;
                }
            }

            listBoxParty.Items.Clear();
            foreach (Pokemon p in PokemonList)
            {
                listBoxParty.Items.Add(p.Name + "  Lv" + p.Level + "  [" + p.CurrentHP+"/"+p.HP+"]");
            }

        }

        private void InitializePokemon()
        {
            datasrc = File.ReadAllLines(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\Pokemon.csv");

            for (int i = 0; i < 151; i++)
            {
                string[] pkmn = datasrc[i+1].Split(',');
                Pokemon p = new Pokemon();
                p.Name = pkmn[1];
                p.ID = Convert.ToInt32(pkmn[0]);
                p.SetType1(pkmn[9]);
                p.SetType2(pkmn[10]);
                AllPokemon.Add(p);

            }
        }

        private void InitializeLocations()
        {
            string[] homepkmn = { "Pidgey"};
            Location home = new Location("Home Town", "Route 1", null, null, null, 2, 5, homepkmn);
            
            string[] routeonepkmn = { "Pidgey", "Rattata" };
            Location routeone = new Location("Route 1", "Town Two", null, "Home Town", null, 2, 5, routeonepkmn);

            string[] towntwopkmn = { "Pidgey", "Pidgeotto", "Rattata" };
            Location towntwo = new Location("Town Two", null, "Route 2", "Route 1", "Town Two Beach", 2, 6, towntwopkmn);

            string[] towntwobeachpkmn = { "Pidgey", "Krabby", "Poliwag", "Magikarp" };
            Location towntwobeach = new Location("Town Two Beach", null, "Town Two", null, null, 3, 7, towntwobeachpkmn);

            string[] routetwopkmn = { "Pidgey", "Pidgeotto", "Rattata", "Meowth", "Spearow"};
            Location routetwo = new Location("Route 2", null, null, null, "Town Two", 4, 7, routetwopkmn);

            LocationList.Add(home);
            LocationList.Add(routeone);
            LocationList.Add(towntwo);
            LocationList.Add(towntwobeach);
            LocationList.Add(routetwo);

            currentLocation = home;
        }
        
        private Pokemon CreateNewPokemon(int i)
        {
            Pokemon poke = new Pokemon();

            string[] pkmn = datasrc[i].Split(',');
            poke.Name = pkmn[1];
            poke.ID = Convert.ToInt32(pkmn[0]);
            poke.SetType1(pkmn[9]);
            poke.SetType2(pkmn[10]);
            poke.EvolvesFrom = pkmn[12];
            poke.CatchRate = Convert.ToInt32(pkmn[13]);
            poke.HP = Convert.ToInt32(pkmn[3]);
            poke.CurrentHP = poke.HP;

            poke.Level = rng.Next(currentLocation.minlevel, currentLocation.maxlevel);


            if (pkmn[11] == "None")
            {
                poke.Gender = Gender.Genderless;
            }
            else
            {
                poke.MaleChance = Convert.ToDouble(pkmn[11]);
                poke.GenerateIndependantValues();
            }

            return poke;
        }
        
        private int GetIDFromName(string poke)
        {
            int i = 1;
            foreach (Pokemon p in AllPokemon)
            {
                if(p.Name == poke)
                {
                    i = p.ID;
                }                      
            }
            return i;
        }

        private void ThrowPokeball(object sender, EventArgs e)
        {
            if (pokeballs > 0)
            {
                pokeballs--;
                if (pkmn.ThrowPokeball())
                {
                    UpdateUI();
                    MessageBox.Show("You caught " + pkmn.Name);
                    
                    PokemonList.Add(pkmn);
                    BattleMode = false;
                    timer.Interval = 1;
                    timer.Start();
                    UpdateUI();
                }
                else
                {
                    UpdateUI();
                    MessageBox.Show("The " + pkmn.Name + " broke out!");
                }
            }
            else
            {
                MessageBox.Show("You do not have any Pokeballs left!");
            }
        }

        private void Shop(object sender, EventArgs e)
        {
            Shop shop = new Shop();
            shop.ShowDialog();
            UpdateUI();
        }

        private void GoNorth(object sender, EventArgs e)
        {
            foreach (Location l in LocationList)
            {
                if(btnNorth.Text == l.name)
                {
                    currentLocation = l;
                    UpdateUI();

                    break;
                }
            }
        }

        private void GoEast(object sender, EventArgs e)
        {
            foreach (Location l in LocationList)
            {
                if (btnEast.Text == l.name)
                {
                    currentLocation = l;
                    UpdateUI();
                    break;
                }
            }
        }

        private void GoSouth(object sender, EventArgs e)
        {
            foreach (Location l in LocationList)
            {
                if (btnSouth.Text == l.name)
                {
                    currentLocation = l;
                    UpdateUI();
                    break;
                }
            }
        }

        private void GoWest(object sender, EventArgs e)
        {
            foreach (Location l in LocationList)
            {
                if (btnWest.Text == l.name)
                {
                    currentLocation = l;
                    UpdateUI();
                    break;
                }
            }
        }

        private void UpdateTracker()
        {
            tracker1.Image = null;
            tracker2.Image = null;
            tracker3.Image = null;
            tracker4.Image = null;
            tracker5.Image = null;
            tracker6.Image = null;

            if (currentLocation.pokemon.Length > 0)
            {
                object imgt1 = Properties.Resources.ResourceManager.GetObject(GetIDFromName(currentLocation.pokemon[0]).ToString());
                tracker1.Image = (Image)imgt1;
            }
            if (currentLocation.pokemon.Length > 1)
            {
                object imgt2 = Properties.Resources.ResourceManager.GetObject(GetIDFromName(currentLocation.pokemon[1]).ToString());
                tracker2.Image = (Image)imgt2;
            }
            if (currentLocation.pokemon.Length > 2)
            {
                object imgt3 = Properties.Resources.ResourceManager.GetObject(GetIDFromName(currentLocation.pokemon[2]).ToString());
                tracker3.Image = (Image)imgt3;
            }
            if (currentLocation.pokemon.Length > 3)
            {
                object imgt4 = Properties.Resources.ResourceManager.GetObject(GetIDFromName(currentLocation.pokemon[3]).ToString());
                tracker4.Image = (Image)imgt4;
            }
            if (currentLocation.pokemon.Length > 4)
            {
                object imgt5 = Properties.Resources.ResourceManager.GetObject(GetIDFromName(currentLocation.pokemon[4]).ToString());
                tracker5.Image = (Image)imgt5;
            }
            if (currentLocation.pokemon.Length > 5)
            {
                object imgt6 = Properties.Resources.ResourceManager.GetObject(GetIDFromName(currentLocation.pokemon[5]).ToString());
                tracker6.Image = (Image)imgt6;
            }
        }

        private void Tackle(object sender, EventArgs e)
        {
            pkmn.CurrentHP = pkmn.CurrentHP - 10;
            if (pkmn.CurrentHP < 0)
            {
                pkmn.CurrentHP = 0;
            }
            if (pkmn.CurrentHP == 0 )
            {
                btnPokeBall.Enabled = false;
                picType1.Image = null;
                picType2.Image = null;
                picType3.Image = null;
                picSprite.Image = null;
                l_appeared.Text = null;
                l_gender.Text = null;
                l_level.Text = null;
                btnGreatBall.Enabled = false;
                UpdateUI();
                MessageBox.Show("The wild " + pkmn.Name + " was defeated.");
            }
        }

        private void Tick(object sender, EventArgs e)
        {
            timer.Interval = rng.Next(100, 5000);
            int uniquePokeInArea = currentLocation.pokemon.Length;

            int i = rng.Next(0, uniquePokeInArea);

            pkmn = CreateNewPokemon(GetIDFromName(currentLocation.pokemon[i]));
            UpdateUI();

        }

        private void Run(object sender, EventArgs e)
        {
            BattleMode = false;
            timer.Interval = 100;
            timer.Start();
            UpdateUI();
        }

        private void Battle(object sender, EventArgs e)
        {
            BattleMode = true;
            timer.Stop();
            UpdateUI();
        }
    }
}
