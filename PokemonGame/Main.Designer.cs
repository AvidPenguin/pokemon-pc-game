﻿namespace PokemonGame
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.l_appeared = new System.Windows.Forms.Label();
            this.l_gender = new System.Windows.Forms.Label();
            this.btnPokeBall = new System.Windows.Forms.Button();
            this.l_pokeballs = new System.Windows.Forms.Label();
            this.l_greatballs = new System.Windows.Forms.Label();
            this.l_ultraballs = new System.Windows.Forms.Label();
            this.l_money = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnShop = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.picPokeballs = new System.Windows.Forms.PictureBox();
            this.picType3 = new System.Windows.Forms.PictureBox();
            this.picType2 = new System.Windows.Forms.PictureBox();
            this.picType1 = new System.Windows.Forms.PictureBox();
            this.picSprite = new System.Windows.Forms.PictureBox();
            this.listBoxParty = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnNorth = new System.Windows.Forms.Button();
            this.btnSouth = new System.Windows.Forms.Button();
            this.btnEast = new System.Windows.Forms.Button();
            this.btnWest = new System.Windows.Forms.Button();
            this.l_location = new System.Windows.Forms.Label();
            this.l_level = new System.Windows.Forms.Label();
            this.btnBattle = new System.Windows.Forms.Button();
            this.btnMove2 = new System.Windows.Forms.Button();
            this.btnMove1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnGreatBall = new System.Windows.Forms.Button();
            this.btnUltraBall = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.boxPokemon = new System.Windows.Forms.GroupBox();
            this.btnMove4 = new System.Windows.Forms.Button();
            this.btnMove3 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tracker1 = new System.Windows.Forms.PictureBox();
            this.tracker2 = new System.Windows.Forms.PictureBox();
            this.tracker4 = new System.Windows.Forms.PictureBox();
            this.tracker3 = new System.Windows.Forms.PictureBox();
            this.tracker6 = new System.Windows.Forms.PictureBox();
            this.tracker5 = new System.Windows.Forms.PictureBox();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.btnManagePokemon = new System.Windows.Forms.Button();
            this.btnRun = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPokeballs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picType3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picType2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picType1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSprite)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.boxPokemon.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tracker1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tracker2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tracker4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tracker3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tracker6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tracker5)).BeginInit();
            this.SuspendLayout();
            // 
            // l_appeared
            // 
            this.l_appeared.Font = new System.Drawing.Font("Microsoft YaHei UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_appeared.Location = new System.Drawing.Point(382, 93);
            this.l_appeared.Name = "l_appeared";
            this.l_appeared.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.l_appeared.Size = new System.Drawing.Size(395, 27);
            this.l_appeared.TabIndex = 4;
            this.l_appeared.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // l_gender
            // 
            this.l_gender.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_gender.Location = new System.Drawing.Point(633, 166);
            this.l_gender.Name = "l_gender";
            this.l_gender.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.l_gender.Size = new System.Drawing.Size(63, 20);
            this.l_gender.TabIndex = 5;
            this.l_gender.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnPokeBall
            // 
            this.btnPokeBall.Enabled = false;
            this.btnPokeBall.Location = new System.Drawing.Point(112, 103);
            this.btnPokeBall.Margin = new System.Windows.Forms.Padding(4);
            this.btnPokeBall.Name = "btnPokeBall";
            this.btnPokeBall.Size = new System.Drawing.Size(160, 34);
            this.btnPokeBall.TabIndex = 8;
            this.btnPokeBall.Text = "Throw Poke Ball";
            this.btnPokeBall.UseVisualStyleBackColor = true;
            this.btnPokeBall.Click += new System.EventHandler(this.ThrowPokeball);
            // 
            // l_pokeballs
            // 
            this.l_pokeballs.Location = new System.Drawing.Point(30, 137);
            this.l_pokeballs.Name = "l_pokeballs";
            this.l_pokeballs.Size = new System.Drawing.Size(65, 19);
            this.l_pokeballs.TabIndex = 11;
            this.l_pokeballs.Text = "10";
            this.l_pokeballs.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // l_greatballs
            // 
            this.l_greatballs.Location = new System.Drawing.Point(30, 193);
            this.l_greatballs.Name = "l_greatballs";
            this.l_greatballs.Size = new System.Drawing.Size(65, 19);
            this.l_greatballs.TabIndex = 13;
            this.l_greatballs.Text = "10";
            this.l_greatballs.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // l_ultraballs
            // 
            this.l_ultraballs.Location = new System.Drawing.Point(30, 252);
            this.l_ultraballs.Name = "l_ultraballs";
            this.l_ultraballs.Size = new System.Drawing.Size(65, 19);
            this.l_ultraballs.TabIndex = 15;
            this.l_ultraballs.Text = "10";
            this.l_ultraballs.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // l_money
            // 
            this.l_money.Location = new System.Drawing.Point(6, 48);
            this.l_money.Name = "l_money";
            this.l_money.Size = new System.Drawing.Size(109, 19);
            this.l_money.TabIndex = 17;
            this.l_money.Text = "£500";
            this.l_money.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(30, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 19);
            this.label4.TabIndex = 18;
            this.label4.Text = "Money:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnShop
            // 
            this.btnShop.Location = new System.Drawing.Point(112, 33);
            this.btnShop.Margin = new System.Windows.Forms.Padding(4);
            this.btnShop.Name = "btnShop";
            this.btnShop.Size = new System.Drawing.Size(66, 34);
            this.btnShop.TabIndex = 19;
            this.btnShop.Text = "Shop";
            this.btnShop.UseVisualStyleBackColor = true;
            this.btnShop.Click += new System.EventHandler(this.Shop);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::PokemonGame.Properties.Resources.ultra_ball;
            this.pictureBox2.Location = new System.Drawing.Point(47, 218);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(30, 30);
            this.pictureBox2.TabIndex = 14;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::PokemonGame.Properties.Resources.great_ball;
            this.pictureBox1.Location = new System.Drawing.Point(47, 159);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(30, 30);
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // picPokeballs
            // 
            this.picPokeballs.Image = global::PokemonGame.Properties.Resources.poke_ball;
            this.picPokeballs.Location = new System.Drawing.Point(47, 103);
            this.picPokeballs.Name = "picPokeballs";
            this.picPokeballs.Size = new System.Drawing.Size(30, 30);
            this.picPokeballs.TabIndex = 10;
            this.picPokeballs.TabStop = false;
            // 
            // picType3
            // 
            this.picType3.Location = new System.Drawing.Point(563, 242);
            this.picType3.Name = "picType3";
            this.picType3.Size = new System.Drawing.Size(32, 12);
            this.picType3.TabIndex = 9;
            this.picType3.TabStop = false;
            // 
            // picType2
            // 
            this.picType2.Location = new System.Drawing.Point(587, 242);
            this.picType2.Name = "picType2";
            this.picType2.Size = new System.Drawing.Size(32, 12);
            this.picType2.TabIndex = 7;
            this.picType2.TabStop = false;
            // 
            // picType1
            // 
            this.picType1.Location = new System.Drawing.Point(541, 242);
            this.picType1.Name = "picType1";
            this.picType1.Size = new System.Drawing.Size(32, 12);
            this.picType1.TabIndex = 6;
            this.picType1.TabStop = false;
            // 
            // picSprite
            // 
            this.picSprite.Location = new System.Drawing.Point(531, 140);
            this.picSprite.Name = "picSprite";
            this.picSprite.Size = new System.Drawing.Size(96, 96);
            this.picSprite.TabIndex = 3;
            this.picSprite.TabStop = false;
            // 
            // listBoxParty
            // 
            this.listBoxParty.FormattingEnabled = true;
            this.listBoxParty.ItemHeight = 19;
            this.listBoxParty.Location = new System.Drawing.Point(19, 64);
            this.listBoxParty.Margin = new System.Windows.Forms.Padding(4);
            this.listBoxParty.Name = "listBoxParty";
            this.listBoxParty.Size = new System.Drawing.Size(325, 118);
            this.listBoxParty.TabIndex = 20;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnShop);
            this.groupBox1.Controls.Add(this.picPokeballs);
            this.groupBox1.Controls.Add(this.btnUltraBall);
            this.groupBox1.Controls.Add(this.l_pokeballs);
            this.groupBox1.Controls.Add(this.btnGreatBall);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.l_money);
            this.groupBox1.Controls.Add(this.l_greatballs);
            this.groupBox1.Controls.Add(this.l_ultraballs);
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.btnPokeBall);
            this.groupBox1.Location = new System.Drawing.Point(828, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(351, 357);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Items";
            // 
            // btnNorth
            // 
            this.btnNorth.Location = new System.Drawing.Point(189, 41);
            this.btnNorth.Margin = new System.Windows.Forms.Padding(4);
            this.btnNorth.Name = "btnNorth";
            this.btnNorth.Size = new System.Drawing.Size(137, 34);
            this.btnNorth.TabIndex = 20;
            this.btnNorth.Text = "NORTH";
            this.btnNorth.UseVisualStyleBackColor = true;
            this.btnNorth.Click += new System.EventHandler(this.GoNorth);
            // 
            // btnSouth
            // 
            this.btnSouth.Location = new System.Drawing.Point(189, 120);
            this.btnSouth.Margin = new System.Windows.Forms.Padding(4);
            this.btnSouth.Name = "btnSouth";
            this.btnSouth.Size = new System.Drawing.Size(137, 34);
            this.btnSouth.TabIndex = 23;
            this.btnSouth.Text = "SOUTH";
            this.btnSouth.UseVisualStyleBackColor = true;
            this.btnSouth.Click += new System.EventHandler(this.GoSouth);
            // 
            // btnEast
            // 
            this.btnEast.Location = new System.Drawing.Point(329, 81);
            this.btnEast.Margin = new System.Windows.Forms.Padding(4);
            this.btnEast.Name = "btnEast";
            this.btnEast.Size = new System.Drawing.Size(137, 34);
            this.btnEast.TabIndex = 24;
            this.btnEast.Text = "EAST";
            this.btnEast.UseVisualStyleBackColor = true;
            this.btnEast.Click += new System.EventHandler(this.GoEast);
            // 
            // btnWest
            // 
            this.btnWest.Location = new System.Drawing.Point(50, 81);
            this.btnWest.Margin = new System.Windows.Forms.Padding(4);
            this.btnWest.Name = "btnWest";
            this.btnWest.Size = new System.Drawing.Size(137, 34);
            this.btnWest.TabIndex = 25;
            this.btnWest.Text = "WEST";
            this.btnWest.UseVisualStyleBackColor = true;
            this.btnWest.Click += new System.EventHandler(this.GoWest);
            // 
            // l_location
            // 
            this.l_location.Location = new System.Drawing.Point(6, 28);
            this.l_location.Name = "l_location";
            this.l_location.Size = new System.Drawing.Size(137, 19);
            this.l_location.TabIndex = 20;
            this.l_location.Text = "<<LOCATION>>";
            this.l_location.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // l_level
            // 
            this.l_level.Font = new System.Drawing.Font("Microsoft YaHei UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_level.Location = new System.Drawing.Point(634, 204);
            this.l_level.Name = "l_level";
            this.l_level.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.l_level.Size = new System.Drawing.Size(63, 20);
            this.l_level.TabIndex = 27;
            this.l_level.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnBattle
            // 
            this.btnBattle.Location = new System.Drawing.Point(117, 220);
            this.btnBattle.Margin = new System.Windows.Forms.Padding(4);
            this.btnBattle.Name = "btnBattle";
            this.btnBattle.Size = new System.Drawing.Size(137, 34);
            this.btnBattle.TabIndex = 1;
            this.btnBattle.Text = "Battle";
            this.btnBattle.UseVisualStyleBackColor = true;
            this.btnBattle.Click += new System.EventHandler(this.Battle);
            // 
            // btnMove2
            // 
            this.btnMove2.Enabled = false;
            this.btnMove2.Location = new System.Drawing.Point(187, 190);
            this.btnMove2.Margin = new System.Windows.Forms.Padding(4);
            this.btnMove2.Name = "btnMove2";
            this.btnMove2.Size = new System.Drawing.Size(158, 34);
            this.btnMove2.TabIndex = 30;
            this.btnMove2.Text = "Water Tail (10)";
            this.btnMove2.UseVisualStyleBackColor = true;
            // 
            // btnMove1
            // 
            this.btnMove1.Enabled = false;
            this.btnMove1.Location = new System.Drawing.Point(21, 190);
            this.btnMove1.Margin = new System.Windows.Forms.Padding(4);
            this.btnMove1.Name = "btnMove1";
            this.btnMove1.Size = new System.Drawing.Size(158, 34);
            this.btnMove1.TabIndex = 31;
            this.btnMove1.Text = "Tackle (20)";
            this.btnMove1.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei UI", 11.25F);
            this.label1.Location = new System.Drawing.Point(17, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(192, 28);
            this.label1.TabIndex = 32;
            this.label1.Text = "My Party";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnGreatBall
            // 
            this.btnGreatBall.Enabled = false;
            this.btnGreatBall.Location = new System.Drawing.Point(112, 160);
            this.btnGreatBall.Margin = new System.Windows.Forms.Padding(4);
            this.btnGreatBall.Name = "btnGreatBall";
            this.btnGreatBall.Size = new System.Drawing.Size(160, 34);
            this.btnGreatBall.TabIndex = 33;
            this.btnGreatBall.Text = "Throw Great Ball";
            this.btnGreatBall.UseVisualStyleBackColor = true;
            // 
            // btnUltraBall
            // 
            this.btnUltraBall.Enabled = false;
            this.btnUltraBall.Location = new System.Drawing.Point(112, 218);
            this.btnUltraBall.Margin = new System.Windows.Forms.Padding(4);
            this.btnUltraBall.Name = "btnUltraBall";
            this.btnUltraBall.Size = new System.Drawing.Size(160, 34);
            this.btnUltraBall.TabIndex = 34;
            this.btnUltraBall.Text = "Throw Ultra Ball";
            this.btnUltraBall.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnRun);
            this.groupBox2.Controls.Add(this.btnSouth);
            this.groupBox2.Controls.Add(this.btnNorth);
            this.groupBox2.Controls.Add(this.btnEast);
            this.groupBox2.Controls.Add(this.btnWest);
            this.groupBox2.Controls.Add(this.l_location);
            this.groupBox2.Controls.Add(this.btnBattle);
            this.groupBox2.Location = new System.Drawing.Point(301, 375);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(521, 283);
            this.groupBox2.TabIndex = 22;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Navigation";
            // 
            // boxPokemon
            // 
            this.boxPokemon.Controls.Add(this.btnManagePokemon);
            this.boxPokemon.Controls.Add(this.btnMove4);
            this.boxPokemon.Controls.Add(this.btnMove3);
            this.boxPokemon.Controls.Add(this.listBoxParty);
            this.boxPokemon.Controls.Add(this.btnMove2);
            this.boxPokemon.Controls.Add(this.btnMove1);
            this.boxPokemon.Controls.Add(this.label1);
            this.boxPokemon.Location = new System.Drawing.Point(828, 375);
            this.boxPokemon.Name = "boxPokemon";
            this.boxPokemon.Size = new System.Drawing.Size(351, 284);
            this.boxPokemon.TabIndex = 22;
            this.boxPokemon.TabStop = false;
            this.boxPokemon.Text = "Pokemon";
            // 
            // btnMove4
            // 
            this.btnMove4.Enabled = false;
            this.btnMove4.Location = new System.Drawing.Point(187, 234);
            this.btnMove4.Margin = new System.Windows.Forms.Padding(4);
            this.btnMove4.Name = "btnMove4";
            this.btnMove4.Size = new System.Drawing.Size(158, 34);
            this.btnMove4.TabIndex = 33;
            this.btnMove4.Text = "Water Tail (10)";
            this.btnMove4.UseVisualStyleBackColor = true;
            // 
            // btnMove3
            // 
            this.btnMove3.Enabled = false;
            this.btnMove3.Location = new System.Drawing.Point(21, 234);
            this.btnMove3.Margin = new System.Windows.Forms.Padding(4);
            this.btnMove3.Name = "btnMove3";
            this.btnMove3.Size = new System.Drawing.Size(158, 34);
            this.btnMove3.TabIndex = 34;
            this.btnMove3.Text = "Tackle (20)";
            this.btnMove3.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tracker6);
            this.groupBox4.Controls.Add(this.tracker5);
            this.groupBox4.Controls.Add(this.tracker4);
            this.groupBox4.Controls.Add(this.tracker3);
            this.groupBox4.Controls.Add(this.tracker2);
            this.groupBox4.Controls.Add(this.tracker1);
            this.groupBox4.Location = new System.Drawing.Point(14, 264);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(281, 395);
            this.groupBox4.TabIndex = 26;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Tracker";
            // 
            // tracker1
            // 
            this.tracker1.Location = new System.Drawing.Point(23, 39);
            this.tracker1.Name = "tracker1";
            this.tracker1.Size = new System.Drawing.Size(96, 96);
            this.tracker1.TabIndex = 28;
            this.tracker1.TabStop = false;
            // 
            // tracker2
            // 
            this.tracker2.Location = new System.Drawing.Point(137, 39);
            this.tracker2.Name = "tracker2";
            this.tracker2.Size = new System.Drawing.Size(96, 96);
            this.tracker2.TabIndex = 29;
            this.tracker2.TabStop = false;
            // 
            // tracker4
            // 
            this.tracker4.Location = new System.Drawing.Point(137, 152);
            this.tracker4.Name = "tracker4";
            this.tracker4.Size = new System.Drawing.Size(96, 96);
            this.tracker4.TabIndex = 31;
            this.tracker4.TabStop = false;
            // 
            // tracker3
            // 
            this.tracker3.Location = new System.Drawing.Point(23, 152);
            this.tracker3.Name = "tracker3";
            this.tracker3.Size = new System.Drawing.Size(96, 96);
            this.tracker3.TabIndex = 30;
            this.tracker3.TabStop = false;
            // 
            // tracker6
            // 
            this.tracker6.Location = new System.Drawing.Point(137, 269);
            this.tracker6.Name = "tracker6";
            this.tracker6.Size = new System.Drawing.Size(96, 96);
            this.tracker6.TabIndex = 33;
            this.tracker6.TabStop = false;
            // 
            // tracker5
            // 
            this.tracker5.Location = new System.Drawing.Point(23, 269);
            this.tracker5.Name = "tracker5";
            this.tracker5.Size = new System.Drawing.Size(96, 96);
            this.tracker5.TabIndex = 32;
            this.tracker5.TabStop = false;
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 10;
            this.timer.Tick += new System.EventHandler(this.Tick);
            // 
            // btnManagePokemon
            // 
            this.btnManagePokemon.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnManagePokemon.Location = new System.Drawing.Point(216, 26);
            this.btnManagePokemon.Margin = new System.Windows.Forms.Padding(4);
            this.btnManagePokemon.Name = "btnManagePokemon";
            this.btnManagePokemon.Size = new System.Drawing.Size(128, 30);
            this.btnManagePokemon.TabIndex = 35;
            this.btnManagePokemon.Text = "Manage Pokemon";
            this.btnManagePokemon.UseVisualStyleBackColor = true;
            // 
            // btnRun
            // 
            this.btnRun.Enabled = false;
            this.btnRun.Location = new System.Drawing.Point(259, 220);
            this.btnRun.Margin = new System.Windows.Forms.Padding(4);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(137, 34);
            this.btnRun.TabIndex = 26;
            this.btnRun.Text = "Run";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.Run);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1191, 670);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.boxPokemon);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.l_level);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.picType3);
            this.Controls.Add(this.picType2);
            this.Controls.Add(this.picType1);
            this.Controls.Add(this.l_gender);
            this.Controls.Add(this.l_appeared);
            this.Controls.Add(this.picSprite);
            this.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Main";
            this.Text = "Pokemon";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPokeballs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picType3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picType2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picType1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSprite)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.boxPokemon.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tracker1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tracker2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tracker4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tracker3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tracker6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tracker5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox picSprite;
        private System.Windows.Forms.Label l_appeared;
        private System.Windows.Forms.Label l_gender;
        private System.Windows.Forms.PictureBox picType1;
        private System.Windows.Forms.PictureBox picType2;
        private System.Windows.Forms.Button btnPokeBall;
        private System.Windows.Forms.PictureBox picType3;
        private System.Windows.Forms.PictureBox picPokeballs;
        private System.Windows.Forms.Label l_pokeballs;
        private System.Windows.Forms.Label l_greatballs;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label l_ultraballs;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnShop;
        public System.Windows.Forms.Label l_money;
        private System.Windows.Forms.ListBox listBoxParty;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnNorth;
        private System.Windows.Forms.Button btnSouth;
        private System.Windows.Forms.Button btnEast;
        private System.Windows.Forms.Button btnWest;
        public System.Windows.Forms.Label l_location;
        private System.Windows.Forms.Label l_level;
        private System.Windows.Forms.Button btnBattle;
        private System.Windows.Forms.Button btnMove2;
        private System.Windows.Forms.Button btnMove1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnGreatBall;
        private System.Windows.Forms.Button btnUltraBall;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnMove4;
        private System.Windows.Forms.Button btnMove3;
        private System.Windows.Forms.GroupBox boxPokemon;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.PictureBox tracker6;
        private System.Windows.Forms.PictureBox tracker5;
        private System.Windows.Forms.PictureBox tracker4;
        private System.Windows.Forms.PictureBox tracker3;
        private System.Windows.Forms.PictureBox tracker2;
        private System.Windows.Forms.PictureBox tracker1;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Button btnManagePokemon;
        private System.Windows.Forms.Button btnRun;
    }
}

