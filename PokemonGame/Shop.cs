﻿using System;
using System.Windows.Forms;

namespace PokemonGame
{
    public partial class Shop : Form
    {
        public Shop()
        {
            InitializeComponent();
            UpdateUI();
        }

        private void UpdateUI()
        {
            l_money.Text = "£" + Main.money.ToString();
            l_pokeballs.Text = Main.pokeballs.ToString();
            l_greatballs.Text = Main.greatballs.ToString();
            l_ultraballs.Text = Main.ultraballs.ToString();
        }

        private void LeaveShop(object sender, EventArgs e)
        {
            Close();
        }

        private void BuyPokeBall1(object sender, EventArgs e)
        {
            if (Main.money>=10)
            {
                Main.pokeballs = Main.pokeballs + 1;
                Main.money = Main.money - 10;
            }
            else
            {
                MessageBox.Show("You do not have enough money for this purchase!");
            }
            UpdateUI();
        }

        private void BuyPokeBall10(object sender, EventArgs e)
        {
            if (Main.money >= 100)
            {
                Main.pokeballs = Main.pokeballs + 10;
                Main.money = Main.money - 100;
            }
            else
            {
                MessageBox.Show("You do not have enough money for this purchase!");
            }
            UpdateUI();
        }

        private void BuyGreatBall1(object sender, EventArgs e)
        {
            if (Main.money >= 30)
            {
                Main.greatballs = Main.greatballs + 1;
                Main.money = Main.money - 30;
            }
            else
            {
                MessageBox.Show("You do not have enough money for this purchase!");
            }
            UpdateUI();
        }

        private void BuyGreatBall10(object sender, EventArgs e)
        {
            if (Main.money >= 300)
            {
                Main.greatballs = Main.greatballs + 10;
                Main.money = Main.money - 300;
            }
            else
            {
                MessageBox.Show("You do not have enough money for this purchase!");
            }
            UpdateUI();
        }

        private void BuyUltraBall1(object sender, EventArgs e)
        {
            if (Main.money >= 60)
            {
                Main.ultraballs = Main.ultraballs + 1;
                Main.money = Main.money - 60;
            }
            else
            {
                MessageBox.Show("You do not have enough money for this purchase!");
            }
            UpdateUI();
        }

        private void BuyUltraBall10(object sender, EventArgs e)
        {
            if (Main.money >= 600)
            {
                Main.ultraballs = Main.ultraballs + 10;
                Main.money = Main.money - 600;
            }
            else
            {
                MessageBox.Show("You do not have enough money for this purchase!");
            }
            UpdateUI();
        }
    }
}
