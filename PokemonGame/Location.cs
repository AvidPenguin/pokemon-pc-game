﻿namespace PokemonGame
{
    public class Location
    {
        public string name { get; set; }
        public string north { get; set; }
        public string east { get; set; }
        public string south { get; set; }
        public string west { get; set; }
        public int minlevel { get; set; }
        public int maxlevel { get; set; }
        public string[] pokemon{ get; set; }

        public Location(string _name, string n, string e, string s, string w, int min, int max, string[] pkmn)
        {
            name = _name;
            north = n;
            east = e;
            south = s;
            west = w;
            pokemon = pkmn;
            minlevel = min;
            maxlevel = max;
        }
    }
}