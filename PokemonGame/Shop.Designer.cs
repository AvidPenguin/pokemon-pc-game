﻿namespace PokemonGame
{
    partial class Shop
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Shop));
            this.btnLeave = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.l_money = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.l_pokeballs = new System.Windows.Forms.Label();
            this.picPokeballs = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPoke1 = new System.Windows.Forms.Button();
            this.btnPoke10 = new System.Windows.Forms.Button();
            this.btnGreat10 = new System.Windows.Forms.Button();
            this.btnGreat1 = new System.Windows.Forms.Button();
            this.btnUltra10 = new System.Windows.Forms.Button();
            this.btnUltra1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.l_greatballs = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.l_ultraballs = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPokeballs)).BeginInit();
            this.SuspendLayout();
            // 
            // btnLeave
            // 
            this.btnLeave.Location = new System.Drawing.Point(117, 250);
            this.btnLeave.Margin = new System.Windows.Forms.Padding(4);
            this.btnLeave.Name = "btnLeave";
            this.btnLeave.Size = new System.Drawing.Size(94, 34);
            this.btnLeave.TabIndex = 28;
            this.btnLeave.Text = "Leave";
            this.btnLeave.UseVisualStyleBackColor = true;
            this.btnLeave.Click += new System.EventHandler(this.LeaveShop);
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(130, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 19);
            this.label4.TabIndex = 27;
            this.label4.Text = "Money:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // l_money
            // 
            this.l_money.Font = new System.Drawing.Font("Microsoft YaHei UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_money.Location = new System.Drawing.Point(15, 28);
            this.l_money.Name = "l_money";
            this.l_money.Size = new System.Drawing.Size(298, 24);
            this.l_money.TabIndex = 26;
            this.l_money.Text = "£500";
            this.l_money.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::PokemonGame.Properties.Resources.ultra_ball;
            this.pictureBox2.Location = new System.Drawing.Point(249, 84);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(30, 30);
            this.pictureBox2.TabIndex = 24;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::PokemonGame.Properties.Resources.great_ball;
            this.pictureBox1.Location = new System.Drawing.Point(151, 84);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(30, 30);
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            // 
            // l_pokeballs
            // 
            this.l_pokeballs.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_pokeballs.Location = new System.Drawing.Point(15, 136);
            this.l_pokeballs.Name = "l_pokeballs";
            this.l_pokeballs.Size = new System.Drawing.Size(94, 19);
            this.l_pokeballs.TabIndex = 21;
            this.l_pokeballs.Text = "10";
            this.l_pokeballs.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picPokeballs
            // 
            this.picPokeballs.Image = global::PokemonGame.Properties.Resources.poke_ball;
            this.picPokeballs.Location = new System.Drawing.Point(48, 84);
            this.picPokeballs.Name = "picPokeballs";
            this.picPokeballs.Size = new System.Drawing.Size(30, 30);
            this.picPokeballs.TabIndex = 20;
            this.picPokeballs.TabStop = false;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 19);
            this.label3.TabIndex = 29;
            this.label3.Text = "Poke Ball";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(15, 117);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 19);
            this.label1.TabIndex = 30;
            this.label1.Text = "You own:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnPoke1
            // 
            this.btnPoke1.Font = new System.Drawing.Font("Microsoft YaHei UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPoke1.Location = new System.Drawing.Point(15, 168);
            this.btnPoke1.Margin = new System.Windows.Forms.Padding(4);
            this.btnPoke1.Name = "btnPoke1";
            this.btnPoke1.Size = new System.Drawing.Size(94, 25);
            this.btnPoke1.TabIndex = 31;
            this.btnPoke1.Text = "Buy 1 (£10)";
            this.btnPoke1.UseVisualStyleBackColor = true;
            this.btnPoke1.Click += new System.EventHandler(this.BuyPokeBall1);
            // 
            // btnPoke10
            // 
            this.btnPoke10.Font = new System.Drawing.Font("Microsoft YaHei UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPoke10.Location = new System.Drawing.Point(15, 201);
            this.btnPoke10.Margin = new System.Windows.Forms.Padding(4);
            this.btnPoke10.Name = "btnPoke10";
            this.btnPoke10.Size = new System.Drawing.Size(94, 25);
            this.btnPoke10.TabIndex = 32;
            this.btnPoke10.Text = "Buy 10 (£100)";
            this.btnPoke10.UseVisualStyleBackColor = true;
            this.btnPoke10.Click += new System.EventHandler(this.BuyPokeBall10);
            // 
            // btnGreat10
            // 
            this.btnGreat10.Font = new System.Drawing.Font("Microsoft YaHei UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGreat10.Location = new System.Drawing.Point(117, 201);
            this.btnGreat10.Margin = new System.Windows.Forms.Padding(4);
            this.btnGreat10.Name = "btnGreat10";
            this.btnGreat10.Size = new System.Drawing.Size(94, 25);
            this.btnGreat10.TabIndex = 34;
            this.btnGreat10.Text = "Buy 10 (£300)";
            this.btnGreat10.UseVisualStyleBackColor = true;
            this.btnGreat10.Click += new System.EventHandler(this.BuyGreatBall10);
            // 
            // btnGreat1
            // 
            this.btnGreat1.Font = new System.Drawing.Font("Microsoft YaHei UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGreat1.Location = new System.Drawing.Point(117, 168);
            this.btnGreat1.Margin = new System.Windows.Forms.Padding(4);
            this.btnGreat1.Name = "btnGreat1";
            this.btnGreat1.Size = new System.Drawing.Size(94, 25);
            this.btnGreat1.TabIndex = 33;
            this.btnGreat1.Text = "Buy 1 (£30)";
            this.btnGreat1.UseVisualStyleBackColor = true;
            this.btnGreat1.Click += new System.EventHandler(this.BuyGreatBall1);
            // 
            // btnUltra10
            // 
            this.btnUltra10.Font = new System.Drawing.Font("Microsoft YaHei UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUltra10.Location = new System.Drawing.Point(219, 201);
            this.btnUltra10.Margin = new System.Windows.Forms.Padding(4);
            this.btnUltra10.Name = "btnUltra10";
            this.btnUltra10.Size = new System.Drawing.Size(94, 25);
            this.btnUltra10.TabIndex = 36;
            this.btnUltra10.Text = "Buy 10 (£600)";
            this.btnUltra10.UseVisualStyleBackColor = true;
            this.btnUltra10.Click += new System.EventHandler(this.BuyUltraBall10);
            // 
            // btnUltra1
            // 
            this.btnUltra1.Font = new System.Drawing.Font("Microsoft YaHei UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUltra1.Location = new System.Drawing.Point(219, 168);
            this.btnUltra1.Margin = new System.Windows.Forms.Padding(4);
            this.btnUltra1.Name = "btnUltra1";
            this.btnUltra1.Size = new System.Drawing.Size(94, 25);
            this.btnUltra1.TabIndex = 35;
            this.btnUltra1.Text = "Buy 1 (£60)";
            this.btnUltra1.UseVisualStyleBackColor = true;
            this.btnUltra1.Click += new System.EventHandler(this.BuyUltraBall1);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(120, 117);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 19);
            this.label2.TabIndex = 40;
            this.label2.Text = "You own:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(117, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 19);
            this.label5.TabIndex = 39;
            this.label5.Text = "Great Ball";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // l_greatballs
            // 
            this.l_greatballs.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_greatballs.Location = new System.Drawing.Point(120, 136);
            this.l_greatballs.Name = "l_greatballs";
            this.l_greatballs.Size = new System.Drawing.Size(94, 19);
            this.l_greatballs.TabIndex = 38;
            this.l_greatballs.Text = "5";
            this.l_greatballs.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(216, 117);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 19);
            this.label7.TabIndex = 44;
            this.label7.Text = "You own:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(213, 62);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 19);
            this.label8.TabIndex = 43;
            this.label8.Text = "Ultra Ball";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // l_ultraballs
            // 
            this.l_ultraballs.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_ultraballs.Location = new System.Drawing.Point(216, 136);
            this.l_ultraballs.Name = "l_ultraballs";
            this.l_ultraballs.Size = new System.Drawing.Size(94, 19);
            this.l_ultraballs.TabIndex = 42;
            this.l_ultraballs.Text = "0";
            this.l_ultraballs.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Shop
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(352, 307);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.l_ultraballs);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.l_greatballs);
            this.Controls.Add(this.btnUltra10);
            this.Controls.Add(this.btnUltra1);
            this.Controls.Add(this.btnGreat10);
            this.Controls.Add(this.btnGreat1);
            this.Controls.Add(this.btnPoke10);
            this.Controls.Add(this.btnPoke1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnLeave);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.l_money);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.l_pokeballs);
            this.Controls.Add(this.picPokeballs);
            this.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Shop";
            this.Text = "Shop";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPokeballs)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnLeave;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label l_pokeballs;
        private System.Windows.Forms.PictureBox picPokeballs;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnPoke1;
        private System.Windows.Forms.Button btnPoke10;
        private System.Windows.Forms.Button btnGreat10;
        private System.Windows.Forms.Button btnGreat1;
        private System.Windows.Forms.Button btnUltra10;
        private System.Windows.Forms.Button btnUltra1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label l_greatballs;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label l_ultraballs;
        private System.Windows.Forms.Label l_money;
    }
}